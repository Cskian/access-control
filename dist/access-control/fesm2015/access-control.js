import { Injectable, Component, NgModule, defineInjectable } from '@angular/core';

/**
 * @fileoverview added by tsickle
 * Generated from: lib/access-control.service.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class AccessControlService {
    constructor() { }
}
AccessControlService.decorators = [
    { type: Injectable, args: [{
                providedIn: 'root'
            },] }
];
/** @nocollapse */
AccessControlService.ctorParameters = () => [];
/** @nocollapse */ AccessControlService.ngInjectableDef = defineInjectable({ factory: function AccessControlService_Factory() { return new AccessControlService(); }, token: AccessControlService, providedIn: "root" });

/**
 * @fileoverview added by tsickle
 * Generated from: lib/access-control.component.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class AccessControlComponent {
    constructor() { }
    /**
     * @return {?}
     */
    ngOnInit() {
    }
}
AccessControlComponent.decorators = [
    { type: Component, args: [{
                selector: 'lib-access-control',
                template: `
    <p>
      access-control works!
    </p>
  `
            }] }
];
/** @nocollapse */
AccessControlComponent.ctorParameters = () => [];

/**
 * @fileoverview added by tsickle
 * Generated from: lib/access-control.module.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class AccessControlModule {
}
AccessControlModule.decorators = [
    { type: NgModule, args: [{
                declarations: [AccessControlComponent],
                imports: [],
                exports: [AccessControlComponent]
            },] }
];

/**
 * @fileoverview added by tsickle
 * Generated from: public-api.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */

/**
 * @fileoverview added by tsickle
 * Generated from: access-control.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */

export { AccessControlService, AccessControlComponent, AccessControlModule };

//# sourceMappingURL=access-control.js.map