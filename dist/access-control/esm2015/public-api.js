/**
 * @fileoverview added by tsickle
 * Generated from: public-api.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*
 * Public API Surface of access-control
 */
export { AccessControlService } from './lib/access-control.service';
export { AccessControlComponent } from './lib/access-control.component';
export { AccessControlModule } from './lib/access-control.module';
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicHVibGljLWFwaS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2FjY2Vzcy1jb250cm9sLyIsInNvdXJjZXMiOlsicHVibGljLWFwaS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7OztBQUlBLHFDQUFjLDhCQUE4QixDQUFDO0FBQzdDLHVDQUFjLGdDQUFnQyxDQUFDO0FBQy9DLG9DQUFjLDZCQUE2QixDQUFDIiwic291cmNlc0NvbnRlbnQiOlsiLypcbiAqIFB1YmxpYyBBUEkgU3VyZmFjZSBvZiBhY2Nlc3MtY29udHJvbFxuICovXG5cbmV4cG9ydCAqIGZyb20gJy4vbGliL2FjY2Vzcy1jb250cm9sLnNlcnZpY2UnO1xuZXhwb3J0ICogZnJvbSAnLi9saWIvYWNjZXNzLWNvbnRyb2wuY29tcG9uZW50JztcbmV4cG9ydCAqIGZyb20gJy4vbGliL2FjY2Vzcy1jb250cm9sLm1vZHVsZSc7XG4iXX0=