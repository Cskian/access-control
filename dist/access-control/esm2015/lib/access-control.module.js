/**
 * @fileoverview added by tsickle
 * Generated from: lib/access-control.module.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { NgModule } from '@angular/core';
import { AccessControlComponent } from './access-control.component';
export class AccessControlModule {
}
AccessControlModule.decorators = [
    { type: NgModule, args: [{
                declarations: [AccessControlComponent],
                imports: [],
                exports: [AccessControlComponent]
            },] }
];
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYWNjZXNzLWNvbnRyb2wubW9kdWxlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vYWNjZXNzLWNvbnRyb2wvIiwic291cmNlcyI6WyJsaWIvYWNjZXNzLWNvbnRyb2wubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7O0FBQUEsT0FBTyxFQUFFLFFBQVEsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUN6QyxPQUFPLEVBQUUsc0JBQXNCLEVBQUUsTUFBTSw0QkFBNEIsQ0FBQztBQVFwRSxNQUFNLE9BQU8sbUJBQW1COzs7WUFOL0IsUUFBUSxTQUFDO2dCQUNSLFlBQVksRUFBRSxDQUFDLHNCQUFzQixDQUFDO2dCQUN0QyxPQUFPLEVBQUUsRUFDUjtnQkFDRCxPQUFPLEVBQUUsQ0FBQyxzQkFBc0IsQ0FBQzthQUNsQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IE5nTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBBY2Nlc3NDb250cm9sQ29tcG9uZW50IH0gZnJvbSAnLi9hY2Nlc3MtY29udHJvbC5jb21wb25lbnQnO1xuXG5ATmdNb2R1bGUoe1xuICBkZWNsYXJhdGlvbnM6IFtBY2Nlc3NDb250cm9sQ29tcG9uZW50XSxcbiAgaW1wb3J0czogW1xuICBdLFxuICBleHBvcnRzOiBbQWNjZXNzQ29udHJvbENvbXBvbmVudF1cbn0pXG5leHBvcnQgY2xhc3MgQWNjZXNzQ29udHJvbE1vZHVsZSB7IH1cbiJdfQ==