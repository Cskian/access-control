import { Injectable, Component, NgModule, defineInjectable } from '@angular/core';

/**
 * @fileoverview added by tsickle
 * Generated from: lib/access-control.service.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
var AccessControlService = /** @class */ (function () {
    function AccessControlService() {
    }
    AccessControlService.decorators = [
        { type: Injectable, args: [{
                    providedIn: 'root'
                },] }
    ];
    /** @nocollapse */
    AccessControlService.ctorParameters = function () { return []; };
    /** @nocollapse */ AccessControlService.ngInjectableDef = defineInjectable({ factory: function AccessControlService_Factory() { return new AccessControlService(); }, token: AccessControlService, providedIn: "root" });
    return AccessControlService;
}());

/**
 * @fileoverview added by tsickle
 * Generated from: lib/access-control.component.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
var AccessControlComponent = /** @class */ (function () {
    function AccessControlComponent() {
    }
    /**
     * @return {?}
     */
    AccessControlComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
    };
    AccessControlComponent.decorators = [
        { type: Component, args: [{
                    selector: 'lib-access-control',
                    template: "\n    <p>\n      access-control works!\n    </p>\n  "
                }] }
    ];
    /** @nocollapse */
    AccessControlComponent.ctorParameters = function () { return []; };
    return AccessControlComponent;
}());

/**
 * @fileoverview added by tsickle
 * Generated from: lib/access-control.module.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
var AccessControlModule = /** @class */ (function () {
    function AccessControlModule() {
    }
    AccessControlModule.decorators = [
        { type: NgModule, args: [{
                    declarations: [AccessControlComponent],
                    imports: [],
                    exports: [AccessControlComponent]
                },] }
    ];
    return AccessControlModule;
}());

/**
 * @fileoverview added by tsickle
 * Generated from: public-api.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */

/**
 * @fileoverview added by tsickle
 * Generated from: access-control.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */

export { AccessControlService, AccessControlComponent, AccessControlModule };

//# sourceMappingURL=access-control.js.map