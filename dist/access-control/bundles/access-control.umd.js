(function (global, factory) {
    typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports, require('@angular/core')) :
    typeof define === 'function' && define.amd ? define('access-control', ['exports', '@angular/core'], factory) :
    (factory((global['access-control'] = {}),global.ng.core));
}(this, (function (exports,i0) { 'use strict';

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/access-control.service.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var AccessControlService = /** @class */ (function () {
        function AccessControlService() {
        }
        AccessControlService.decorators = [
            { type: i0.Injectable, args: [{
                        providedIn: 'root'
                    },] }
        ];
        /** @nocollapse */
        AccessControlService.ctorParameters = function () { return []; };
        /** @nocollapse */ AccessControlService.ngInjectableDef = i0.defineInjectable({ factory: function AccessControlService_Factory() { return new AccessControlService(); }, token: AccessControlService, providedIn: "root" });
        return AccessControlService;
    }());

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/access-control.component.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var AccessControlComponent = /** @class */ (function () {
        function AccessControlComponent() {
        }
        /**
         * @return {?}
         */
        AccessControlComponent.prototype.ngOnInit = /**
         * @return {?}
         */
            function () {
            };
        AccessControlComponent.decorators = [
            { type: i0.Component, args: [{
                        selector: 'lib-access-control',
                        template: "\n    <p>\n      access-control works!\n    </p>\n  "
                    }] }
        ];
        /** @nocollapse */
        AccessControlComponent.ctorParameters = function () { return []; };
        return AccessControlComponent;
    }());

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/access-control.module.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var AccessControlModule = /** @class */ (function () {
        function AccessControlModule() {
        }
        AccessControlModule.decorators = [
            { type: i0.NgModule, args: [{
                        declarations: [AccessControlComponent],
                        imports: [],
                        exports: [AccessControlComponent]
                    },] }
        ];
        return AccessControlModule;
    }());

    /**
     * @fileoverview added by tsickle
     * Generated from: public-api.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */

    /**
     * @fileoverview added by tsickle
     * Generated from: access-control.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */

    exports.AccessControlService = AccessControlService;
    exports.AccessControlComponent = AccessControlComponent;
    exports.AccessControlModule = AccessControlModule;

    Object.defineProperty(exports, '__esModule', { value: true });

})));

//# sourceMappingURL=access-control.umd.js.map