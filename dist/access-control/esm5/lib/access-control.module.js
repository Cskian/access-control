/**
 * @fileoverview added by tsickle
 * Generated from: lib/access-control.module.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { NgModule } from '@angular/core';
import { AccessControlComponent } from './access-control.component';
var AccessControlModule = /** @class */ (function () {
    function AccessControlModule() {
    }
    AccessControlModule.decorators = [
        { type: NgModule, args: [{
                    declarations: [AccessControlComponent],
                    imports: [],
                    exports: [AccessControlComponent]
                },] }
    ];
    return AccessControlModule;
}());
export { AccessControlModule };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYWNjZXNzLWNvbnRyb2wubW9kdWxlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vYWNjZXNzLWNvbnRyb2wvIiwic291cmNlcyI6WyJsaWIvYWNjZXNzLWNvbnRyb2wubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7O0FBQUEsT0FBTyxFQUFFLFFBQVEsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUN6QyxPQUFPLEVBQUUsc0JBQXNCLEVBQUUsTUFBTSw0QkFBNEIsQ0FBQztBQUVwRTtJQUFBO0lBTW1DLENBQUM7O2dCQU5uQyxRQUFRLFNBQUM7b0JBQ1IsWUFBWSxFQUFFLENBQUMsc0JBQXNCLENBQUM7b0JBQ3RDLE9BQU8sRUFBRSxFQUNSO29CQUNELE9BQU8sRUFBRSxDQUFDLHNCQUFzQixDQUFDO2lCQUNsQzs7SUFDa0MsMEJBQUM7Q0FBQSxBQU5wQyxJQU1vQztTQUF2QixtQkFBbUIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBOZ01vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgQWNjZXNzQ29udHJvbENvbXBvbmVudCB9IGZyb20gJy4vYWNjZXNzLWNvbnRyb2wuY29tcG9uZW50JztcblxuQE5nTW9kdWxlKHtcbiAgZGVjbGFyYXRpb25zOiBbQWNjZXNzQ29udHJvbENvbXBvbmVudF0sXG4gIGltcG9ydHM6IFtcbiAgXSxcbiAgZXhwb3J0czogW0FjY2Vzc0NvbnRyb2xDb21wb25lbnRdXG59KVxuZXhwb3J0IGNsYXNzIEFjY2Vzc0NvbnRyb2xNb2R1bGUgeyB9XG4iXX0=