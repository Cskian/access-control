/**
 * @fileoverview added by tsickle
 * Generated from: lib/access-control.component.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component } from '@angular/core';
var AccessControlComponent = /** @class */ (function () {
    function AccessControlComponent() {
    }
    /**
     * @return {?}
     */
    AccessControlComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
    };
    AccessControlComponent.decorators = [
        { type: Component, args: [{
                    selector: 'lib-access-control',
                    template: "\n    <p>\n      access-control works!\n    </p>\n  "
                }] }
    ];
    /** @nocollapse */
    AccessControlComponent.ctorParameters = function () { return []; };
    return AccessControlComponent;
}());
export { AccessControlComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYWNjZXNzLWNvbnRyb2wuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vYWNjZXNzLWNvbnRyb2wvIiwic291cmNlcyI6WyJsaWIvYWNjZXNzLWNvbnRyb2wuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7O0FBQUEsT0FBTyxFQUFFLFNBQVMsRUFBVSxNQUFNLGVBQWUsQ0FBQztBQUVsRDtJQVdFO0lBQWdCLENBQUM7Ozs7SUFFakIseUNBQVE7OztJQUFSO0lBQ0EsQ0FBQzs7Z0JBZEYsU0FBUyxTQUFDO29CQUNULFFBQVEsRUFBRSxvQkFBb0I7b0JBQzlCLFFBQVEsRUFBRSxzREFJVDtpQkFFRjs7OztJQVFELDZCQUFDO0NBQUEsQUFoQkQsSUFnQkM7U0FQWSxzQkFBc0IiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6ICdsaWItYWNjZXNzLWNvbnRyb2wnLFxuICB0ZW1wbGF0ZTogYFxuICAgIDxwPlxuICAgICAgYWNjZXNzLWNvbnRyb2wgd29ya3MhXG4gICAgPC9wPlxuICBgLFxuICBzdHlsZXM6IFtdXG59KVxuZXhwb3J0IGNsYXNzIEFjY2Vzc0NvbnRyb2xDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xuXG4gIGNvbnN0cnVjdG9yKCkgeyB9XG5cbiAgbmdPbkluaXQoKSB7XG4gIH1cblxufVxuIl19