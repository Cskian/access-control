/*
 * Public API Surface of access-control
 */

export * from './lib/access-control.service';
export * from './lib/access-control.component';
export * from './lib/access-control.module';
