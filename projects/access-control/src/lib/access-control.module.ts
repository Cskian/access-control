import { NgModule } from '@angular/core';
import { AccessControlComponent } from './access-control.component';

@NgModule({
  declarations: [AccessControlComponent],
  imports: [
  ],
  exports: [AccessControlComponent]
})
export class AccessControlModule { }
